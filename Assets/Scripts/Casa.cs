﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casa : MonoBehaviour {
	public bool isOcupada;
	public bool isJogador1;
	public int x, y;
	public Casa(int x, int y){
		this.x = x;
		this.y = y;
	}
}
