﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DadosPartida : MonoBehaviour {
	public double volume;

	public Tema tema;

	public Tema TemaAtual(){
		return this.tema;
	}

	public void EscolherTemaAtual(Tema tema){
		this.tema = tema;
	}
}
