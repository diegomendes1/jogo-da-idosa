﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jogo : MonoBehaviour {
	public DadosPartida dados;
	[SerializeField]
	private Tela tela;

	private bool isJogador1;
	Tabuleiro tabuleiro;

	void Start () {
		isJogador1 = true;
		dados = GameObject.Find("DadosPartida").GetComponent<DadosPartida>();
		tabuleiro = new Tabuleiro();
		IniciarJogo();
	}

	//Aqui o jogo realmente vai iniciar, com os ajustes iniciais e afins.
	private void IniciarJogo(){
		tabuleiro.GerarTabuleiro();
		tela.EncontrarTema(dados.tema);
		tela.GerarTabuleiroUI();
	}

	public void VerificarJogada(int x, int y){
		if(tabuleiro.CasaVazia(x, y))
			Jogar(x, y);
	}

	public void Jogar(int x, int y){
		tabuleiro.Jogar(x, y, isJogador1);
		if(isJogador1){
			tela.Jogar(x, y, dados.tema.xTema);
		}else{
			tela.Jogar(x, y, dados.tema.oTema);
		}

		if(VerificarEmpate()){
			FimDeJogoEmpate();
		}else{
			List<Casa> listaVitoria = VerificarVitoria(x, y);
			if(listaVitoria != null){
				FimDeJogo(listaVitoria);
			}else{
				TrocarJogador();
			}
		}
	}

	public List<Casa> VerificarVitoria(int x, int y){
		List<Casa> resultado = VerificarVitoriaHor(x, 0);
		if(resultado.Count!= 3){
			resultado = VerificarVitoriaVer(0, y);
			if(resultado.Count!=3){
				resultado = VerificarVitoriaDiag(x, y);
				if(resultado.Count!=3){
					return null;
				}
			}
		}
		return resultado;
	}

	public List<Casa> VerificarVitoriaHor(int x, int y){
		List<Casa> lista = new List<Casa>();
		if(!tabuleiro.CasaVazia(x, y) && tabuleiro.MesmoJogador(x, y, isJogador1)){
			lista.Add(tabuleiro.GetCasa(x, y));
		}
		if(y == 2){
			return lista;
		}else{
			List<Casa> recursividade = VerificarVitoriaHor(x, y+1);
			foreach(Casa c in recursividade){
				lista.Add(c);
			}
			return lista;
		}
	}

	public List<Casa> VerificarVitoriaVer(int x, int y){
		List<Casa> lista = new List<Casa>();
		if(!tabuleiro.CasaVazia(x, y) && tabuleiro.MesmoJogador(x, y, isJogador1)){
			lista.Add(tabuleiro.GetCasa(x, y));
		}
		if(x == 2){
			return lista;
		}else{
			List<Casa> recursividade = VerificarVitoriaVer(x+1, y);
			foreach(Casa c in recursividade){
				lista.Add(c);
			}
			return lista;
		}
	}

	public List<Casa> VerificarVitoriaDiag(int x, int y){
		List<Casa> resultado = VerificarVitoriaDiagAscendente(2, 0);
			if(resultado.Count!=3){
				resultado = VerificarVitoriaDiagDescendente(0, 0);
				if(resultado.Count!=3){
					return new List<Casa>();
				}
			}
		return resultado;
	}

	public List<Casa> VerificarVitoriaDiagAscendente(int x, int y){
		List<Casa> lista = new List<Casa>();
		if(x+y==2 && !tabuleiro.CasaVazia(x, y) && tabuleiro.MesmoJogador(x, y, isJogador1)){
			lista.Add(tabuleiro.GetCasa(x, y));
		}
		if(x == 0){
			return lista;
		}else{
			List<Casa> recursividade = VerificarVitoriaDiagAscendente(x-1, y+1);
			foreach(Casa c in recursividade){
				lista.Add(c);
			}
			return lista;
		}
	}

	public List<Casa> VerificarVitoriaDiagDescendente(int x, int y){
		List<Casa> lista = new List<Casa>();
		if(x==y && !tabuleiro.CasaVazia(x, y) && tabuleiro.MesmoJogador(x, y, isJogador1)){
			lista.Add(tabuleiro.GetCasa(x, y));
		}
		if(x == 2){
			return lista;
		}else{
			List<Casa> recursividade = VerificarVitoriaDiagDescendente(x+1, y+1);
			foreach(Casa c in recursividade){
				lista.Add(c);
			}
			return lista;
		}
	}

	public bool VerificarEmpate(){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(tabuleiro.CasaVazia(i,j))
					return false;
			}
		}
		return true;
	}
	public void FimDeJogoEmpate(){	
		tela.FimJogoEmpate();
	}

	public void FimDeJogo(List<Casa> listaVitoria){
		if(isJogador1){
			tela.FimJogo(true);
		}else{
			tela.FimJogo(false);
		
		}
		tela.MostrarListaVitoria(listaVitoria);
	}

	public void Desistir(){
		if(isJogador1){
			tela.FimJogo(false);
		}else{
			tela.FimJogo(true);
		}
	}

	public void Sair(){
		Destroy(dados.gameObject);
		SceneManager.LoadScene(0);
	}

	public void Reiniciar(){
		SceneManager.LoadScene(1);
	}

	public void TrocarJogador(){
		if(isJogador1){
			isJogador1 = false;
		}else{
			isJogador1 = true;
		}
		tela.TrocarJogador(isJogador1);
	}
}
