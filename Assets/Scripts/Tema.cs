﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tema : MonoBehaviour {

	//Aqui ficam os dados do tema, referencias para imagens, etc

	public Sprite fundoTema;
	public Sprite xTema;
	public Sprite oTema;
	public string nomeTema;
	public Color corFundo;
}
