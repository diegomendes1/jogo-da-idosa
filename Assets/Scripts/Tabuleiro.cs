﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tabuleiro : MonoBehaviour {

	Casa[,] tabuleiro;

	public void GerarTabuleiro(){
		tabuleiro = new Casa[3,3];

		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				tabuleiro[i,j] = new Casa(i, j);
			}
		}
	}	

	public bool CasaVazia(int x, int y) {
		return !tabuleiro[x, y].isOcupada;
	}

	public void Jogar(int x, int y, bool isJogador1){
		tabuleiro[x,y].isOcupada = true;
		tabuleiro[x,y].isJogador1 = isJogador1;
	}

	public bool MesmoJogador(int x, int y, bool isJogador1){
		return (tabuleiro[x, y].isJogador1 == isJogador1);
	}

	public Casa GetCasa(int x, int y){
		return tabuleiro[x, y];
	}
}
