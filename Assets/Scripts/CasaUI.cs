﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CasaUI : MonoBehaviour {

	public int x=0, y=0;

	public void SetCoordenadas(int novoX, int novoY){
		x = novoX;
		y = novoY;
	}

	public void SetImagem(Sprite imagem){
		GetComponent<Image>().sprite = imagem;
	}
}
