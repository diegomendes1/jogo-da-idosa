﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tela : MonoBehaviour {
	//Variaveis do tema
	public GameObject tabuleiroUI;
	public GameObject casaPrefab;
	public GameObject info;
	public GameObject infoEmpate;
	public GameObject infoVitoria;
	public Image infoJogadorVencedor;
	public GameObject menuSuperior;
	public GameObject menuSuperiorFimJogo;

	public Image fundoJogo;
	public Image jogador1;
	public Image jogador2;
	public Jogo jogo;

	CasaUI[,] tabuleiroCasasUI;


	//Carregar os dados iniciais do tema.
	public void EncontrarTema(Tema novoTema){
		fundoJogo.color = novoTema.corFundo;
		tabuleiroUI.GetComponent<Image>().overrideSprite = novoTema.fundoTema;
		jogador1.overrideSprite = novoTema.xTema;
		jogador2.overrideSprite = novoTema.oTema;

		MostrarJogador(jogador1);
		EsconderJogador(jogador2);
		info.SetActive(true);
		infoEmpate.SetActive(false);
		infoVitoria.SetActive(false);
		menuSuperior.SetActive(true);
		menuSuperiorFimJogo.SetActive(false);
	}

	public void VoltarMenu(){
		Destroy(GameObject.Find("DadosPartida"));
		SceneManager.LoadScene(0);
	}

	public void GerarTabuleiroUI(){
		tabuleiroCasasUI = new CasaUI[3,3];

		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				GameObject novaCasa = Instantiate(casaPrefab);
				novaCasa.GetComponent<CasaUI>().SetCoordenadas(i,j);
				novaCasa.transform.SetParent(tabuleiroUI.transform);
				novaCasa.GetComponent<Button>().onClick.AddListener(() => jogo.VerificarJogada(novaCasa.GetComponent<CasaUI>().x,novaCasa.GetComponent<CasaUI>().y));
				novaCasa.transform.localScale = new Vector3(1,1,1);
				tabuleiroCasasUI[i,j] = novaCasa.GetComponent<CasaUI>();
			}
		}
	}

	public void Jogar(int x, int y, Sprite imagem){
		tabuleiroCasasUI[x, y].SetImagem(imagem);
	}

	public void TrocarJogador(bool isJogador1){
		if(isJogador1){
			MostrarJogador(jogador1);
			EsconderJogador(jogador2);
		}else{
			MostrarJogador(jogador2);
			EsconderJogador(jogador1);
		}
	}

	public void MostrarJogador(Image jogador){
		Color outraCor = jogador.color;
		outraCor.a = 1f;
		jogador.color = outraCor;
	}

	public void EsconderJogador(Image jogador){
		Color outraCor = jogador.color;
		outraCor.a = 0.3f;
		jogador.color = outraCor;
	}

	public void FimJogo(bool jogador1Vencedor){
		BloquearTabuleiro();
		//Esconder info e mostrar info fim de jogo
		info.SetActive(false);
		infoVitoria.SetActive(true);
		menuSuperior.SetActive(false);
		menuSuperiorFimJogo.SetActive(true);
		if(jogador1Vencedor){
			Debug.Log("Quem ganhou foi o jogador 1");
			infoJogadorVencedor.overrideSprite = jogo.dados.tema.xTema;
		}else{
			Debug.Log("Quem ganhou foi o jogador 2");
			infoJogadorVencedor.overrideSprite = jogo.dados.tema.oTema;
		}
	}

	public void FimJogoEmpate(){
		BloquearTabuleiro();
		//Esconder info e mostrar info Empate
		Debug.Log("Empate!");
		info.SetActive(false);
		infoEmpate.SetActive(true);
		menuSuperior.SetActive(false);
		menuSuperiorFimJogo.SetActive(true);
	}

	public void BloquearTabuleiro(){
		foreach(Transform c in tabuleiroUI.transform){
			c.GetComponent<Button>().interactable = false;
		}
	}

	public void MostrarListaVitoria(List<Casa> listaVitoria){
		foreach(Casa casa in listaVitoria){
			tabuleiroCasasUI[casa.x, casa.y].GetComponent<Button>().interactable = true;
		}
	}
}
